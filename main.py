import logging
import json
import yaml
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

###########
# LOGGING #
###########
logging.basicConfig(filename='logs.log',filemode='w', level=logging.INFO, format='%(levelname)s - %(message)s')

###############
# LOAD CONFIG #
###############
with open('config.yaml') as file:
    CONFIG = yaml.safe_load(file)

##############
# LOAD USERS #
##############

with open("users.json") as file:
    data = json.load(file)

def run_server(credentials, config):
    op = webdriver.ChromeOptions()
    op.add_argument('--headless')

    driver = webdriver.Chrome(executable_path="chromedriver/chromedriver", options=op)
    driver.get(config['website'])

    # Sign In
    SIGN_IN = driver.find_element_by_xpath("/html/body/div[1]/div/a")
    SIGN_IN.click()

    # Inputs
    EMAIL_INPUT = driver.find_element_by_id("email")
    PASSWORD_INPUT = driver.find_element_by_id("password")

    # Checkboxes
    CHECKBOX_RULE = driver.find_element_by_id("checkbox-rule")
    CHECKBOX_PRIVACY = driver.find_element_by_id("checkbox-privacy")
    CHECKBOX_COPYRIGHT = driver.find_element_by_id("checkbox-copyright")

    # Button
    LOGIN_BTN = driver.find_element_by_xpath("/html/body/div/form/button")

    # Form Action
    EMAIL_INPUT.send_keys(credentials["UserEmail"])
    PASSWORD_INPUT.send_keys(credentials["UserPassword"])

    # Checkbox Action
    CHECKBOX_RULE.click()
    CHECKBOX_PRIVACY.click()
    CHECKBOX_COPYRIGHT.click()

    # Login
    LOGIN_BTN.click()

    # Loading
    element = False
    try:
        element = WebDriverWait(driver, config['timeout']).until(
            EC.title_contains(("Home Page - Select or create a notebook")))
    except:
        logging.info(f"{credentials['UserEmail']} - Problem with server")

    if element:
        logging.info(f"{credentials['UserEmail']} - Server on")
    driver.close()

for user in data['users']:
    print(f"Start user: {user['UserEmail']}")
    run_server(credentials=user, config=CONFIG)
