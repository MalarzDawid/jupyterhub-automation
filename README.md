# JupyterHub - Automation

# Installing:
1. Create virtualenv
2. `pip install -r requirements.txt`
3. Download `chromium` webdriver  [click](https://sites.google.com/a/chromium.org/chromedriver/downloads) and unzip
4. Rename unzip directory `chromedriver/`
5. Create users.json 
    ```
    {
    "users": [
        {
        "UserEmail": "useremail@gmail.com",
        "UserPassword": "userpassword"
        },
        {
        "UserEmail": "useremail2@email.com",
        "UserPassword": "userpassword"
        }]
    }
    ```
6. Run main.py
